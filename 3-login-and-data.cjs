/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.
*/

const fs = require("fs");
const path = require("path");

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function createFile() {
  fs.writeFile(`file${1}.json`, `File content `, (err) => {
    if (err) {
      console.error(new Error(err));
    } else {
      console.log(`File${1} created`);
    }
  });
  fs.writeFile(`file${2}.json`, `File content `, (err) => {
    if (err) {
      console.error(new Error(err));
    } else {
      console.log(`File${2} created`);
    }
  });
}
createFile();

function deletefiles(noOfFiles, secondsToDelete) {
  return new Promise((resolve, reject) => {
    if (noOfFiles) {
      resolve();
    } else {
      reject("Error : invalid number of files");
    }
  })
    .then(() => {
      setTimeout(() => {
        deleteEachFile("1")
          .then((message) => {
            console.log(message);
            return deleteEachFile("2");
          })
          .then((message) => {
            console.log(message);
          })
          .catch((err) => {
            console.error(new Error(err));
          });
      }, secondsToDelete * 1000);
    })
    .catch((err) => {
      console.error(new Error(err));
    });
}

deletefiles(2, 2);

function deleteEachFile(filename) {
  return new Promise((resolve, reject) => {
    fs.unlink(`file${filename}.json`, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(`File${filename} deleted`);
      }
    });
  });
}

/* Q2. Create a new file with lipsum data (you can google and get this).
Do File Read and write data to another file
Delete the original file
Using promise chaining*/

function lipsumReadWriteDelete(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        console.log("File read successfully");
        resolve(data);
      }
    });
  })
    .then((data) => {
      return new Promise((resolve, reject) => {
        fs.writeFile("FileCopy.txt", data, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve("File written successfully");
          }
        });
      });
    })
    .then((message) => {
      console.log(message);
      return new Promise((resolve, reject) => {
        fs.unlink(filepath, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve("Deleted original file");
          }
        });
      });
    })
    .then((message) => {
      console.log(message);
    })
    .catch((err) => {
      console.error(new Error(err));
    });
}
lipsumReadWriteDelete("lipsum.txt");

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
*/

// function login(user, val) {
//   if (val % 2 === 0) {
//     return Promise.resolve(user);
//   } else {
//     return Promise.reject(new Error("User not found"));
//   }
// }

// function getData() {
//   return Promise.resolve([
//     {
//       id: 1,
//       name: "Test",
//     },
//     {
//       id: 2,
//       name: "Test 2",
//     },
//   ]);
// }

// function logData(user, activity) {
//   // use promises and fs to save activity in some file
// }
